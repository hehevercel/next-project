import { Flex } from "@chakra-ui/react";

export default function Blog() {
    return (

        <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            minH="80vh"
        >
            Blog
        </Flex>

    )
}
