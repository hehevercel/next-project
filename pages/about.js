import { Flex } from "@chakra-ui/react";

export default function About() {
    return (

        <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            minH="80vh"
        >
            About
        </Flex>

    )
}
