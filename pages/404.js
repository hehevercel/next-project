import {
    Flex,
    Heading,
    Button
} from "@chakra-ui/react";
import Link from "next/link";

export default function Error() {
    return (
        <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            minH="80vh"
        >
            <Heading my="5">
                Oops! Page not found.
            </Heading>
            <Link href="/">
                <Button>
                    Back to Home
                </Button>
            </Link>
        </Flex>
    )
}
