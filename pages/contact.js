import { Flex } from "@chakra-ui/react";

export default function Contact() {
    return (

        <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            minH="80vh"
        >
            Contact
        </Flex>

    )
}
