import Header from "./header";
import Footer from "./footer";
import Container from "./container";

export default function Layout({ children }) {
    return (
        <>
            <Header />
            <Container as='main'>
                {children}
            </Container>
            <Footer />
        </>
    )
}
