import {
    Button
} from "@chakra-ui/react"

import Link from "next/link"

export default function Logo() {
    return (
        <>
            <Link href="/">
                <Button>Logo</Button>
            </Link>
        </>
    )
}