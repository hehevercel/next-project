import NextLink from "next/link"
import { Link as ChakraLink } from "@chakra-ui/react"
import { useRouter } from 'next/router'


export default function Link(props) {
    const { children, href, ...rest } = props
    const router = useRouter()
    const isActive = router.pathname === href

    return (
        <>
            <NextLink href={href} passHref>
                <ChakraLink
                    {...rest}
                    color={
                        isActive
                            ?
                            'blue.200'
                            :
                            'inherit'
                    }
                >
                    {children}
                </ChakraLink>
            </NextLink>
        </>
    )
}
