import { Box, Flex } from '@chakra-ui/react'

import Container from "../container";
import Logo from "./logo";
import { Navbar } from "./navbar";
import ThemeSwitch from "./themeswitch";

export default function Home() {
    return (
        <>
            <Box as="header" position="sticky" top="0" backdropFilter="blur(10px)">
                <Container>
                    <Flex as="nav" justifyContent="space-between" alignItems="center" h="10vh">
                        <Logo />
                        <Navbar />
                        <ThemeSwitch />
                    </Flex>
                </Container>
            </Box>
        </>
    )
}