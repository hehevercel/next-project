import {
    Box,
    Menu,
    MenuButton,
    MenuItem,
    MenuList,
    IconButton,
    HStack
} from "@chakra-ui/react"

import {
    HamburgerIcon,
    CloseIcon
} from "@chakra-ui/icons"

import menuitems from "./navitem"
import Link from "./link"

const NavMobile = () => {
    return (
        <Menu
            placement="bottom-end"
            w="0"
        >
            {({ isOpen }) => (
                <>
                    <MenuButton
                        display={[
                            'block',
                            null,
                            'none'
                        ]}
                        ml="auto"
                        mr="4"
                        as={IconButton}
                        aria-label="Options"
                        icon={
                            isOpen
                                ?
                                <CloseIcon
                                    w="3"
                                    h="3"
                                    mb="1"
                                />
                                :
                                <HamburgerIcon
                                    w="5"
                                    h="5"
                                />
                        }
                    />
                    <MenuList
                        display={[
                            'block',
                            null,
                            'none'
                        ]}
                        minW="0"
                    >
                        {menuitems.map(
                            (
                                {
                                    name,
                                    href
                                }
                            ) => (
                                <MenuItem
                                    key={name}
                                    p="0"
                                    _focus={{ bg: 'none' }}
                                    _hover={{ bg: 'none' }}
                                >
                                    <Link
                                        href={href}
                                        px="10"
                                        py="2"
                                        _hover={{
                                            textDecoration: "none",
                                            color: "blue.200"
                                        }}
                                    >
                                        {name}
                                    </Link>
                                </MenuItem>
                            ))}
                    </MenuList>
                </>
            )}
        </Menu>
    )
}

const NavDesktop = () => {
    return (
        <HStack
            as="ul"
            ml="auto"
            spacing="10"
            mr="10"
            display={[
                'none',
                null,
                'flex'
            ]}
        >
            {menuitems.map(
                (
                    {
                        name,
                        href
                    }
                ) => (
                    <Box
                        as="li"
                        listStyleType="none"
                        key={name}
                    >
                        <Link
                            href={href}
                            _hover={{
                                textDecoration: "none",
                                color: "blue.200"
                            }}
                        >
                            {name}
                        </Link>
                    </Box>
                ))}
        </HStack>
    )
}

export function Navbar() {
    return (
        <>
            <NavDesktop />
            <NavMobile />
        </>
    )
}