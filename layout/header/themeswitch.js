import {
    Tooltip,
    IconButton,
    useColorMode,
    useColorModeValue,
} from "@chakra-ui/react"

import {
    MoonIcon,
    SunIcon,
} from "@chakra-ui/icons"

export default function ThemeSwitch() {
    const { toggleColorMode } = useColorMode();
    const themeIcon = useColorModeValue(
        <MoonIcon
            w="5"
            h="5"
        />,
        <SunIcon
            w="5"
            h="5"
        />
    );

    return (

        <Tooltip
            label={useColorModeValue(
                "dark theme",
                "light theme"
            )}
        >
            <IconButton
                onClick={toggleColorMode}
                icon={themeIcon} />
        </Tooltip>

    )
}