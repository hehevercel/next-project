import { Box, Container, Flex } from "@chakra-ui/react";

export default function Footer() {
    return (
        <>
            <Box as="footer" position="sticky" bottom="0" backdropFilter="blur(10px)">
                <Container maxW="container.md">
                    <Flex justifyContent="center" alignItems="center" h="10vh">
                        Noor © 2012 - 2021
                    </Flex>
                </Container>
            </Box>
        </>
    )
}
