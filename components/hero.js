import React from "react";
import {
    Flex,
    Heading,
    Text,
    Button,
    useToast
} from "@chakra-ui/react";

export default function Hero() {
    const toast = useToast()

    return (
        <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            minH="80vh"
        >
            <Heading>
                Hello World!
            </Heading>
            <Text
                fontSize="md"
                my="5"
            >
                Welcome to the Website.
            </Text>
            <Flex>
                <Button
                    onClick={() =>
                        toast({
                            title: "Hello World!",
                            description: "Welcome to the Website.",
                            status: "success",
                            duration: 1000,
                            isClosable: true,
                        })
                    }
                >
                    Toast
                </Button>
            </Flex>
        </Flex>
    )
}
